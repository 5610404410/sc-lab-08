package Model;

public interface Measurable {
	double measure();
}
