package Model;

public class Data {	
	
public double average(Measurable[] meas) {	
	double sum = 0;		
	for (Measurable measurable : meas) {	
		sum = sum + measurable.measure();
	}		
	if (meas.length > 0) { 
		return sum / meas.length; 
	} 		
	else { 
		return 0; 
	}
}
public Measurable min(Measurable m1, Measurable m2){
	if (m1.measure() < m2.measure()){
		return m1;
	}
	else{
		return m2;
	}	
}
}

