package Model;

import java.util.ArrayList;

public class TaxCalculator {
	
	public double sum(ArrayList<Taxable> taxList){
		double total = 0;
		for (Taxable x : taxList){
			total += x.getTax();
		}
		return total;
	}
}
