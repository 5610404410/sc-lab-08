package Model;

public class Company implements Taxable{
	private String companyName;
	private double income;
	private double outcome;
	private double profit;
	
	public Company(String comN, double in, double out){
		companyName = comN;
		income = in;
		outcome = out;
	}
	
	public String getCompanyName(){
		return companyName;
	}
	
	public double getTax(){
		profit = income-outcome;
		return profit * (0.3);
	}
}
