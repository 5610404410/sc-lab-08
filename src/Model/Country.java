package Model;

public class Country implements Measurable{
	private String name;	
	private double area;
	private int numPeople;
	private double gdp;

	public Country(String na, double a, int n, double g) {	
		name = na;	
		area = a;		
		numPeople = n;	
		gdp = g;	
	}

	@Override
	public double measure() {
		return area;
	}

}

