package Model;

public class BankAccount implements Measurable{
	private double balance; 	
	
	public BankAccount(double initialBalance) {  		
		balance = initialBalance;
	}
		
	public void deposit(double amount) {  		
		balance = balance + amount;
	}
		
	public void withdraw(double amount) {  			
		if (balance < amount) 
				
	throw new IllegalArgumentException();			
		balance = balance - amount;
	}

	@Override
	public double measure() {
		return balance;
	}



}

