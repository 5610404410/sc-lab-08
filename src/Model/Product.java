package Model;

public class Product implements Taxable{
	private String productName;
	private double price;
	
	public Product(String proN, double p){
		productName = proN;
		price = p;
	}
	
	public String getProductName(){
		return productName;
	}
	
	public double getTax(){
		return price * (0.07);
	}
}
