package Model;

public class Person implements Measurable,Taxable{
	private String name;
	private double height;
	private double incomePerYear;
	private double tax;
	
	public Person(String na, double h){
		name = na;
		height = h;
	}
	
	public Person(double in){
		incomePerYear = in;
	}
	
	public double getTax(){
		if (incomePerYear <= 300000){
			tax = incomePerYear * (0.05);
		}
		else{
			tax = (300000 * (0.05)) + ((incomePerYear-300000) * (0.1));
		}
		return tax;
	}
	
	@Override
	public double measure() {
		return height;
	}
}
