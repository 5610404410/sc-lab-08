package Main;

import java.util.ArrayList;

import Model.BankAccount;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.Person;
import Model.Product;
import Model.TaxCalculator;
import Model.Taxable;

public class Main {
	public static void main(String[] args) {
		Person[] h = new Person[] {
				new Person("A", 180),
				new Person("B", 156),
				new Person("C", 160),
				new Person("D", 154)
		};		
		
		Data d = new Data();
		System.out.println("Average Height : " + d.average(h));
		
		BankAccount b1 = new BankAccount(50000);
		BankAccount b2 = new BankAccount(20000);		
		System.out.println("Minimum balance: " + d.min(b1, b2).measure());
		
		Country c1 = new Country("Switzerland", 513000, 70, 387);
		Country c2 = new Country("Japan", 236800, 7, 11.14);		
		System.out.println("Minimum area: " + d.min(c1, c2).measure());
		
		Person p1 = new Person("John",178);
		Person p2 = new Person("Neil",185);
		System.out.println("Minimum height: " + d.min(p1, p2).measure());
		
		ArrayList<Taxable> LPer = new ArrayList<Taxable>();
		Person per1 = new Person(250000);
		Person per2 = new Person(320000);
		Person per3 = new Person(600000);
		LPer.add(per1);
		LPer.add(per2);
		LPer.add(per3);
		
		ArrayList<Taxable> LPro = new ArrayList<Taxable>();
		Product pro1 = new Product("TV", 1000);
		Product pro2 = new Product("Fan", 500);
		Product pro3 = new Product("Air", 800);
		LPro.add(pro1);
		LPro.add(pro2);
		LPro.add(pro3);
		
		
		ArrayList<Taxable> LCom = new ArrayList<Taxable>();
		Company com1 = new Company("A", 250000, 50000);
		Company com2 = new Company("B", 320000, 300000);
		Company com3 = new Company("C", 600000, 150000);
		LCom.add(com1);
		LCom.add(com2);
		LCom.add(com3);
		
		ArrayList<Taxable> Ltotal = new ArrayList<Taxable>();
		Ltotal.add(per1);
		Ltotal.add(pro2);
		Ltotal.add(com3);
		
		TaxCalculator t = new TaxCalculator();
		System.out.println("Sum Person's Tax : " + t.sum(LPer));
		System.out.println("Sum Product's Tax : " + t.sum(LPro));
		System.out.println("Sum Company's Tax : " + t.sum(LCom));
		System.out.println("Sum total of Tax : " + t.sum(Ltotal));
		
		
	}
}
